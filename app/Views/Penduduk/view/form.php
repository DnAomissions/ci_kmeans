<form class="card z-depth-3" action="<?=(($data ?? '') != '') ? route_to('penduduk.update', $data->id) : route_to('penduduk.store')?>" method="post">
    <?= csrf_field() ?>
    <div class="card-content row">
        <div class="col s12 m8 offset-m2">
            <div class="input-field" style="display:none;">
                <input name="id" id="id" value="<?= $data->id ?? ''?>" type="text" class="validate" readonly>
                <label for="id">Penduduk ID</label>
            </div>
            <div class="input-field">
                <input name="nik" id="nik" value="<?= (old('nik')) ? old('nik') : $data->nik ?? ''?>" type="text" class="validate" required autofocus>
                <label for="nik">NIK</label>
            </div>
            <div class="input-field">
                <input name="nama" id="nama" value="<?= (old('nama')) ? old('nama') : $data->nama ?? ''?>" type="text" class="validate" required autofocus>
                <label for="nama">Nama</label>
            </div>
            <div class="input-field">
                <input name="umur" id="umur" value="<?= (old('umur')) ? old('umur') : $data->umur ?? ''?>" type="number" class="validate" required>
                <label for="umur">Umur</label>
            </div>
            <div class="input-field">
                <select name="kredit" id="kredit" class="validate" required>
                    <option value="1" <?= (old('kredit') && old('kredit') == '1') ? 'selected' : ''?><?=(($data->kredit ?? '') == "1") ? 'selected' : ''?>>Ya</option>
                    <option value="0" <?= (old('kredit') && old('kredit') == '0') ? 'selected' : ''?><?=(($data->kredit ?? '') == "0") ? 'selected' : ''?>>Tidak</option>
                </select>
                <label for="kredit">Menerima Kredit atau pembiayaan dari Perbankan</label>
            </div>
            <div class="input-field">
                <select name="pekerjaan" id="pekerjaan" class="validate" required>
                    <option value="1" <?= (old('pekerjaan') && old('pekerjaan') == '1') ? 'selected' : ''?><?=(($data->pekerjaan ?? '') == "1") ? 'selected' : ''?>>PNS</option>
                    <option value="2" <?= (old('pekerjaan') && old('pekerjaan') == '2') ? 'selected' : ''?><?=(($data->pekerjaan ?? '') == "2") ? 'selected' : ''?>>Wirausaha</option>
                    <option value="3" <?= (old('pekerjaan') && old('pekerjaan') == '3') ? 'selected' : ''?><?=(($data->pekerjaan ?? '') == "3") ? 'selected' : ''?>>Wiraswasta</option>
                    <option value="4" <?= (old('pekerjaan') && old('pekerjaan') == '4') ? 'selected' : ''?><?=(($data->pekerjaan ?? '') == "4") ? 'selected' : ''?>>Tidak Bekerja</option>
                </select>
                <label for="pekerjaan">Pekerjaan</label>
            </div>
            <div class="input-field">
                <select name="penghasilan" id="penghasilan" class="validate" required>
                    <option value="1" <?= (old('penghasilan') && old('penghasilan') == '1') ? 'selected' : ''?><?=(($data->penghasilan ?? '') == "1") ? 'selected' : ''?>>> Rp. 5.000.000</option>
                    <option value="2" <?= (old('penghasilan') && old('penghasilan') == '2') ? 'selected' : ''?><?=(($data->penghasilan ?? '') == "2") ? 'selected' : ''?>>Rp. 3.000.000 < Rp. 5.000.000</option>
                    <option value="3" <?= (old('penghasilan') && old('penghasilan') == '3') ? 'selected' : ''?><?=(($data->penghasilan ?? '') == "3") ? 'selected' : ''?>>Rp. 1.000.000 < Rp. 3.000.000</option>
                    <option value="4" <?= (old('penghasilan') && old('penghasilan') == '4') ? 'selected' : ''?><?=(($data->penghasilan ?? '') == "4") ? 'selected' : ''?>>Rp. 500.000 < Rp. 1.000.000</option>
                    <option value="5" <?= (old('penghasilan') && old('penghasilan') == '5') ? 'selected' : ''?><?=(($data->penghasilan ?? '') == "5") ? 'selected' : ''?>>< Rp. 500.000</option>
                </select>
                <label for="penghasilan">Penghasilan</label>
            </div>
            <div class="input-field">
                <select name="listrik" id="listrik" class="validate" required>
                    <option value="1" <?= (old('listrik') && old('listrik') == '1') ? 'selected' : ''?><?=(($data->listrik ?? '') == "1") ? 'selected' : ''?>>> 1.300 watt</option>
                    <option value="2" <?= (old('listrik') && old('listrik') == '2') ? 'selected' : ''?><?=(($data->listrik ?? '') == "2") ? 'selected' : ''?>>> 1.200 watt</option>
                    <option value="3" <?= (old('listrik') && old('listrik') == '3') ? 'selected' : ''?><?=(($data->listrik ?? '') == "3") ? 'selected' : ''?>>< 1.200 watt</option>
                    <option value="4" <?= (old('listrik') && old('listrik') == '4') ? 'selected' : ''?><?=(($data->listrik ?? '') == "4") ? 'selected' : ''?>>< 900 watt</option>
                    <option value="5" <?= (old('listrik') && old('listrik') == '5') ? 'selected' : ''?><?=(($data->listrik ?? '') == "5") ? 'selected' : ''?>>< 750 watt</option>
                </select>
                <label for="listrik">Listrik</label>
            </div>
            <div class="input-field">
                <select name="kondisi_rumah" id="kondisi_rumah" class="validate" required>
                    <option value="1" <?= (old('kondisi_rumah') && old('kondisi_rumah') == '1') ? 'selected' : ''?><?=(($data->kondisi_rumah ?? '') == "1") ? 'selected' : ''?>>Sangat Mampu</option>
                    <option value="2" <?= (old('kondisi_rumah') && old('kondisi_rumah') == '2') ? 'selected' : ''?><?=(($data->kondisi_rumah ?? '') == "2") ? 'selected' : ''?>>Mampu</option>
                    <option value="3" <?= (old('kondisi_rumah') && old('kondisi_rumah') == '3') ? 'selected' : ''?><?=(($data->kondisi_rumah ?? '') == "3") ? 'selected' : ''?>>Kurang Mampu</option>
                    <option value="4" <?= (old('kondisi_rumah') && old('kondisi_rumah') == '4') ? 'selected' : ''?><?=(($data->kondisi_rumah ?? '') == "4") ? 'selected' : ''?>>Tidak Mampu</option>
                </select>
                <label for="kondisi_rumah">Kondisi Rumah</label>
            </div>
        </div>
    </div>
    <div class="card-action row">
        <div class="col s12 m8 offset-2">
            <button id="button-submit" type="submit" name="<?=(($data ?? '') != '') ? 'update' : 'store'?>" class="btn green"><?=(($data ?? '') != '') ? 'Update' : 'Save'?></button>
        </div>
    </div>
</form>

<?= $this->section('script') ?>

<?= $this->endSection() ?>
