<?php

namespace App\Models;

use CodeIgniter\Model as BaseModel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Model extends BaseModel
{
    /**
     * Import & Export
     */

    public function export($name = null)
    {
        $columns = $this->allowedFields;
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $alphabet = range('A', 'Z');
        $REC = [];
        foreach($columns as $COL => $c)
        {
            $sheet->setCellValue($alphabet[$COL].'1', $c);
            $REC += [$alphabet[$COL].'1' => $c];
        }

        $data = $this->asArray()->findAll();
        $i = 2;
        foreach($data as $row)
        {
            foreach($columns as $COL => $c)
            {
                $sheet->setCellValue($alphabet[$COL].$i, $row[$c]);
            }
            $i++;
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save($this->table.'.xlsx');
        return true;
    }

    public function import($filepath, $ext)
    {        
        $inputFileType = ucfirst($ext);
        $inputFileName = $filepath;

        /**  Create a new Reader of the type defined in $inputFileType  **/
        $reader = IOFactory::createReader($inputFileType);
        /**  Advise the Reader that we only want to load cell data  **/
        // $reader->setReadDataOnly(true);
        /**  Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = $reader->load($inputFileName);

        $worksheet = $spreadsheet->getActiveSheet();
        $rows = $worksheet->toArray();

        // Delete file after get data
        unlink($inputFileName);
        
        $column_name = $rows[0];
        unset($rows[0]);
        
        if(!$this->checkImportFormat($column_name, $this->allowedFields))
        {
            return false;
        }

        $res = [];
        foreach ($rows as $key => $value) {
            $record = [];
            foreach($column_name as $i => $column){
                $record += $this->setKeyValue(trim($column), $value[$i]);
            }
            $res[] = $record;
        }

        $res = $this->createBatch($res);

        return $res;
    }

    public function checkImportFormat($column_name, $columns_check, $columns_check_alternative = [])
    {
        foreach($column_name as $c)
        {
            $c = trim($c);
            if(!in_array($c, $columns_check))
            {
                if(count($columns_check_alternative) > 0)
                {
                    if(!in_array($c, $columns_check_alternative))
                    {
                        $_SESSION['error'] = "Excel Format not valid!";
                        return false;
                    }
                }else{
                    $_SESSION['error'] = "Excel Format not valid!";
                    return false;
                }
            }
        }

        return true;
    }

    public function createBatch($array)
    {
        $res = [];
        foreach($array as $row)
        {
            if(!$res = $this->save($row))
            {
                $_SESSION['errors'] = $this->errors();
                return false;
            }
        }

        return $res;
    }

    public function setKeyValue($key, $value)
    {
        return [$key => $value];
    }
}
