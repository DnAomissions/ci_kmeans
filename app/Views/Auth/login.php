<?= $this->extend('App\Views\Layouts\auth') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col s12 m6 offset-m3">
            <form id="login_form" method="post" action="<?=route_to('login')?>">
                <?= csrf_field() ?>
                <div class="card z-depth-3">
                    <div class="card-content"> 
                        <span class="card-title" style="font-weight:400;">Sign In</span>
                        <div class="input-field">
                            <input id="login" name="login" type="text" class="validate" value="<?= old('login') ?>" required>
                            <label for="login">Username</label>
                        <?php
                            if(session('errors.login')){
                        ?>
                                <span class="helper-text"><?= session('errors.login') ?></span>
                        <?php
                            }
                        ?>
                        </div>
                        <div class="input-field">
                            <input id="password" name="password" type="password" class="validate" required>
                            <label for="password">Password</label>
                        <?php
                            if(session('errors.password')){
                        ?>
                                <span class="helper-text"><?= session('errors.password') ?></span>
                        <?php
                            }
                        ?>
                        </div>
                        <div class="row">
                            <div class="col sm6">
                                <p>
                                    <label>
                                        <input type="checkbox" name="remember" <?php if(old('remember')) : ?> checked <?php endif ?>/>
                                        <span>Remember Me?</span>
                                    </label>
                                </p>
                            </div>
                            <div class="col sm6" style="float:right;">
                                <a href="<?=route_to('register')?>">Don't have account?</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn">Sign In</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>