<?php

function array_avg($array)
{
    if(count($array) > 0)
    {
        $array_filtered = array_filter($array, function($v){
            return is_numeric($v);
        });
        return array_sum($array_filtered)/count($array_filtered);
    }else{
        return 0;
    }
}

function eucDistance(array $a, array $b) {
    return
    array_sum(
        array_map(
            function($x, $y) {
                return abs($x - $y) ** 2;
            }, $a, $b
        )
    ) ** (1/2);
}