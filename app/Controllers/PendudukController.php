<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\CenteroidPendudukModel;
use App\Models\PendudukModel;

class PendudukController extends BaseController
{
	function __construct()
	{
		helper('array');
	}

	public function proses_data()
	{
		$pendudukModel = new PendudukModel();
		$centeroidPendudukModel = new CenteroidPendudukModel();
		
		$euc = [];

		if(count($centeroidPendudukModel->findAll()) > 0)
		{
			$centeroid = $centeroidPendudukModel->getCenteroid();
			$last_cluster = [];
			$loop = true;
			$iteration = 1;
			$iterationView = [];

			while($loop)
			{
				$euc = $pendudukModel->getEuclidian($centeroid);
				$iterationView[] = [
					'iteration' => $iteration,
					'centeroid' => $centeroid,
					'table_centeroid_id' => 'table-centeroid-'.$iteration,
					'euc' => $euc,
					'table_euclidean_id' => 'table-euclidean-'.$iteration,
				];
				$centeroid = $pendudukModel->getNewCenteroid($euc);
				
				$new_cluster = [];
				foreach ($euc as $row) {
					$new_cluster[] = $row['cluster'];
				}
				if($last_cluster === $new_cluster)
				{
					$loop = false;
				}else{
					$last_cluster = $new_cluster;
					$iteration++;
				}
				
				$euc = $pendudukModel->getEuclidian($centeroid);
				$data['euc'] = $euc;
				$data['iterationView'] = $iterationView;
			}
		}else{
			$centeroid = $centeroidPendudukModel->findAll();
			$data['iterationView'] = [];
		}
		
		if (count($euc) > 0) {
			foreach ($euc as $row)
			{
				$update = [
					'hasil' => 'Tidak Lulus Seleksi',
				];

				if($row['cluster'] === 'C1')
				{
					$update = [
						'hasil' => 'Lulus Seleksi',
					];
				}

				if (!$pendudukModel->update($row['data_id'], $update))
				{
					return redirect()->back()->withInput()->with('errors', $pendudukModel->errors());
				}
			}
		}

		$data['penduduks'] = $pendudukModel->findAll();
		$data['centeroidPenduduk'] = $centeroid;

		return view('Penduduk/proses_data', $data);
	}

	public function proses_data_action()
	{
		$pendudukModel = new PendudukModel();
		$centeroidPendudukModel = new CenteroidPendudukModel();

		if (!$centeroidPendudukModel->where('user_id', user()->id)->delete())
		{
			return redirect()->back()->withInput()->with('errors', $centeroidPendudukModel->errors());
		}

		$pendudukModel->getFirstCenteroid();

		// Success!
		return redirect()->route('penduduk.proses_data')->with('message', 'Success Proses Data Penduduk');
	}

	public function index()
	{
		$pendudukModel = new PendudukModel();
		if(user()->role == 'admin') {
			$data['penduduks'] = $pendudukModel->findAll();
		} else {
			$data['penduduks'] = $pendudukModel->where('user_id', user()->id)->findAll();
		}

		return view('Penduduk/index', $data);
	}

	public function create()
	{
		return view('Penduduk/create');
	}

	public function store()
	{
		$pendudukModel = new PendudukModel();
		
		// Validate basics first since some password rules rely on these fields
		$rules = [
			'nik' 			=> "required|alpha_numeric_space|min_length[16]|max_length[20]|is_unique[data_penduduk.nik]",
			'nama'			=> "required",
			'umur'			=> "required",
			'kredit'		=> "required",
			'pekerjaan'		=> "required",
			'penghasilan'	=> "required",
			'listrik'		=> "required",
			'kondisi_rumah'	=> "required",
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}
		
		$val = [
			$this->request->getPost('kredit'),
			$this->request->getPost('pekerjaan'),
			$this->request->getPost('penghasilan'),
			$this->request->getPost('listrik'),
			$this->request->getPost('kondisi_rumah'),
		];

		$request = [
			'nik' 			=> $this->request->getPost('nik'),
			'nama' 			=> $this->request->getPost('nama'),
			'umur' 			=> $this->request->getPost('umur'),
			'kredit' 		=> $this->request->getPost('kredit'),
			'pekerjaan' 	=> $this->request->getPost('pekerjaan'),
			'penghasilan' 	=> $this->request->getPost('penghasilan'),
			'listrik' 		=> $this->request->getPost('listrik'),
			'kondisi_rumah'	=> $this->request->getPost('kondisi_rumah'),
			'average'		=> array_avg($val),
			'user_id'		=> user()->id,
			'hasil'			=> 'Dalam Proses Seleksi'
		];
		
		// dd($request);
		if (!$pendudukModel->save($request))
		{
			return redirect()->back()->withInput()->with('errors', $pendudukModel->errors());
		}

		// Success!
		return redirect()->route('penduduk.index')->with('message', 'Success create New Data Penduduk');
	}

	public function edit($id)
	{
		$pendudukModel = new PendudukModel();
        $data['penduduk'] = $pendudukModel->find($id);
		if($data['penduduk']['user_id'] != user()->id) {
			// Warning!
			return redirect()->route('penduduk.index')->with('warning', 'Access denied.');
		}
		return view('Penduduk/edit', $data);
	}

	public function update($id)
	{
		$pendudukModel = new PendudukModel();
		
		// Validate basics first since some password rules rely on these fields
		$rules = [
			'nik' 			=> "required|alpha_numeric_space|min_length[16]|max_length[20]|is_unique[data_penduduk.nik,id,{$id}]",
			'nama'			=> "required",
			'umur'			=> "required",
			'kredit'		=> "required",
			'pekerjaan'		=> "required",
			'penghasilan'	=> "required",
			'listrik'		=> "required",
			'kondisi_rumah'	=> "required",
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}
		
		$val = [
			$this->request->getPost('kredit'),
			$this->request->getPost('pekerjaan'),
			$this->request->getPost('penghasilan'),
			$this->request->getPost('listrik'),
			$this->request->getPost('kondisi_rumah'),
		];

		$request = [
			'nik' 			=> $this->request->getPost('nik'),
			'nama' 			=> $this->request->getPost('nama'),
			'umur' 			=> $this->request->getPost('umur'),
			'kredit' 		=> $this->request->getPost('kredit'),
			'pekerjaan' 	=> $this->request->getPost('pekerjaan'),
			'penghasilan' 	=> $this->request->getPost('penghasilan'),
			'listrik' 		=> $this->request->getPost('listrik'),
			'kondisi_rumah'	=> $this->request->getPost('kondisi_rumah'),
			'average'		=> array_avg($val),
		];

		if (!$pendudukModel->update($id, $request))
		{
			return redirect()->back()->withInput()->with('errors', $pendudukModel->errors());
		}

		// Success!
		return redirect()->route('penduduk.index')->with('message', 'Success create New Data Penduduk');
	}

	public function destroy($id)
	{
		$pendudukModel = new PendudukModel();
        
		if (!$pendudukModel->delete($id))
		{
			$pendudukModel->purgeDeleted();
			return redirect()->back()->withInput()->with('errors', $pendudukModel->errors());
		}

		// Success!
		return redirect()->route('penduduk.index')->with('message', 'Success delete Data Penduduk');
	}

	public function truncate()
	{
		$db      = \Config\Database::connect();
		$builder = $db->table('data_penduduk');

		if (!$builder->truncate())
		{
			return redirect()->back()->withInput()->with('errors', $db->error());
		}

		$builder = $db->table('centeroid_penduduk');

		if (!$builder->truncate())
		{
			return redirect()->back()->withInput()->with('errors', $db->error());
		}

		// Success!
		return redirect()->route('penduduk.index')->with('message', 'Success clear all Data Produksi!');
	}

	public function export()
	{
		$pendudukModel = new PendudukModel();
		if(!$pendudukModel->export())
		{
			return redirect()->back()->withInput()->with('errors', $pendudukModel->errors());
		}
		$SESSION['message'] = 'Success export Data Penduduk';
        echo "<a href='/".$pendudukModel->table.".xlsx' id='downloadExport' download/></a>";
        echo "<script>document.getElementById('downloadExport').click()</script>";
		echo "<script>window.location.replace('".route_to('penduduk.index')."')</script>";

		// Success!
		// return redirect()->route('penduduk.index')->with('message', 'Success export Data Penduduk');
	}

	public function import()
	{
		$pendudukModel = new PendudukModel();

		$file = $this->request->getFile('file_import');

		if (! $file->isValid())
		{
			return redirect()->back()->withInput()->with('errors', $file->getErrorString().'('.$file->getError().')');
		}

		if(!$pendudukModel->import($file->getTempName(), $file->getClientExtension()))
		{
			return redirect()->back()->withInput()->with('errors', $pendudukModel->errors());
		}
		// $SESSION['message'] = 'Success export Data Penduduk';
        // echo "<a href='/".$pendudukModel->table.".xlsx' id='downloadExport' download/></a>";
        // echo "<script>document.getElementById('downloadExport').click()</script>";
		// echo "<script>window.location.replace('".route_to('penduduk.index')."')</script>";

		// Success!
		return redirect()->route('penduduk.index')->with('message', 'Success import Data Penduduk');
	}
}
