<?php

namespace App\Controllers;

use App\Models\PendudukModel;

class Home extends BaseController
{
	public function index()
	{
		$pendudukModel = new PendudukModel();
		
		if(user()->role == 'admin') {
			$data['hasil'] = $pendudukModel->findAll();
		} else {
			$data['hasil'] = $pendudukModel->where('user_id', user()->id)->findAll();
		}
		return view('home', $data);
	}
}
