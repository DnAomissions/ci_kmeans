<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateDataPenduduk extends Migration
{
	public function up()
	{
		/*
         * Data Penduduk
         */
        $this->forge->addField([
            'id'               	=> ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
            'nik'			   	=> ['type' => 'varchar', 'constraint' => 20],
			'nama'			   	=> ['type' => 'varchar', 'constraint' => 255],
			'umur'				=> ['type' => 'int', 'default' => 0],
			'kredit'			=> ['type' => 'float', 'default' => 0],
			'pekerjaan'			=> ['type' => 'float', 'default' => 0],
			'penghasilan'		=> ['type' => 'float', 'default' => 0],
			'listrik'			=> ['type' => 'float', 'default' => 0],
			'kondisi_rumah'		=> ['type' => 'float', 'default' => 0],
			'average'			=> ['type' => 'float', 'default' => 0],
            'hasil'             => ['type' => 'varchar', 'constraint' => 255, 'null' => true],
            'user_id'           => ['type' => 'int', 'constraint' => 11],
            'created_at'       	=> ['type' => 'datetime', 'null' => true],
            'updated_at'       	=> ['type' => 'datetime', 'null' => true],
            'deleted_at'       	=> ['type' => 'datetime', 'null' => true],
        ]);

        $this->forge->addKey('id', true);
        $this->forge->addUniqueKey('nik');

        $this->forge->createTable('data_penduduk', true);

		/*
         * Centeroid Penduduk
         */
        $this->forge->addField([
            'id'               	=> ['type' => 'int', 'constraint' => 11, 'unsigned' => true, 'auto_increment' => true],
            'cluster'			=> ['type' => 'varchar', 'constraint' => 20],
			'kredit'			=> ['type' => 'float', 'default' => 0],
			'pekerjaan'			=> ['type' => 'float', 'default' => 0],
			'penghasilan'		=> ['type' => 'float', 'default' => 0],
			'listrik'			=> ['type' => 'float', 'default' => 0],
			'kondisi_rumah'		=> ['type' => 'float', 'default' => 0],
            'user_id'           => ['type' => 'int', 'constraint' => 11],
            'created_at'       	=> ['type' => 'datetime', 'null' => true],
            'updated_at'       	=> ['type' => 'datetime', 'null' => true],
            'deleted_at'       	=> ['type' => 'datetime', 'null' => true],
        ]);

        $this->forge->addKey('id', true);

        $this->forge->createTable('centeroid_penduduk', true);
	}

	public function down()
	{
		$this->forge->dropTable('data_penduduk', true);
		$this->forge->dropTable('centeroid_penduduk', true);
	}
}
