<?php

namespace Config;

use Myth\Auth\Config\Auth as BaseConfig;

class Auth extends BaseConfig
{
    /**
	 * --------------------------------------------------------------------
	 * Views used by Auth Controllers
	 * --------------------------------------------------------------------
	 *
	 * @var array
	 */
	public $views = [
		'login'		   => 'App\Views\Auth\login',
		'register'		=> 'App\Views\Auth\register',
		'forgot'		  => 'Myth\Auth\Views\forgot',
		'reset'		   => 'Myth\Auth\Views\reset',
		'emailForgot'	 => 'Myth\Auth\Views\emails\forgot',
		'emailActivation' => 'Myth\Auth\Views\emails\activation',
	];

	/**
	 * --------------------------------------------------------------------
	 * Additional Fields for "Nothing Personal"
	 * --------------------------------------------------------------------
	 *
	 * The `NothingPersonalValidator` prevents personal information from
	 * being used in passwords. The email and username fields are always
	 * considered by the validator. Do not enter those field names here.
	 *
	 * An extend User Entity might include other personal info such as
	 * first and/or last names. `$personalFields` is where you can add
	 * fields to be considered as "personal" by the NothingPersonalValidator.
	 *
	 * For example:
	 *	 $personalFields = ['firstname', 'lastname'];
	 *
	 * @var string[]
	 */
	public $personalFields = [
		'name', 'role'
	];

    /**
	 * --------------------------------------------------------------------
	 * Require Confirmation Registration via Email
	 * --------------------------------------------------------------------
	 *
	 * When enabled, every registered user will receive an email message
	 * with an activation link to confirm the account.
	 *
	 * @var string|null Name of the ActivatorInterface class
	 */
	public $requireActivation = null;

    /**
	 * --------------------------------------------------------------------
	 * Allow Persistent Login Cookies (Remember me)
	 * --------------------------------------------------------------------
	 *
	 * While every attempt has been made to create a very strong protection
	 * with the remember me system, there are some cases (like when you
	 * need extreme protection, like dealing with users financials) that
	 * you might not want the extra risk associated with this cookie-based
	 * solution.
	 *
	 * @var bool
	 */
	public $allowRemembering = true;
}