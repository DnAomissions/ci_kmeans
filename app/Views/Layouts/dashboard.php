<?= $this->extend('App\Views\Layouts\app') ?>

<!-- Start Style -->
<?= $this->section('pageStyles') ?>
    <link rel="stylesheet" href="/assets/datatable/datatable.css">
    <?= $this->renderSection('style') ?>
<?= $this->endSection() ?>
<!-- End Style -->

<!-- Start App -->
<?= $this->section('app') ?>
    <!-- Dropdown Structure -->
        <!-- Dropdown Analisa -->
        <ul id="dropdown_analisa" class="dropdown-content">
            <li><a href="<?= route_to('penduduk.index') ?>">Data Penduduk</a></li>
        </ul>
        <!-- Dropdown Admin -->
        <ul id="dropdown_admin" class="dropdown-content">
    <?php
        if(user()->role == 'admin')
        {
    ?>
            <li><a href="<?= route_to('users.index') ?>">Users</a></li>
    <?php
        }
    ?>
        </ul>
        <!-- Dropdown User -->
        <ul id="dropdown_user" class="dropdown-content">
            <li><a href="<?= base_url('logout') ?>"><i class="material-icons left">logout</i> Logout</a></li>
        </ul>
    <nav>
        <div class="nav-wrapper">
            <ul class="left hide-on-med-and-down">
                <li><a href="<?= base_url('/') ?>"><i class="material-icons left">home</i> Home</a></li>
                <?php
                    if(user()->role == 'admin')
                    {
                ?>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown_admin"><i class="material-icons left">person</i> Admin <i class="material-icons right">arrow_drop_down</i></a></li>
                <?php
                    }
                ?>
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown_analisa"><i class="material-icons left">insert_chart</i> Analisa <i class="material-icons right">arrow_drop_down</i></a></li>
            </ul>
            <ul class="right hide-on-med-and-down">
                <!-- Dropdown Trigger -->
                <li><a class="dropdown-trigger" href="#!" data-target="dropdown_user"><?=user()->name?><i class="material-icons right">arrow_drop_down</i></a></li>
            </ul>
        </div>
    </nav>

    <!-- Start Content -->
    <br/>
    <div style="width:95%; margin:0 auto;">
        <?= $this->renderSection('content') ?>
    </div>

<?= $this->endSection() ?>
<!-- End App -->

<!-- Start Script -->
<?= $this->section('pageScripts') ?>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script src="/assets/datatable/datatable.js"></script>

    <?= $this->renderSection('script') ?>
<?= $this->endSection() ?>
<!-- End Script -->