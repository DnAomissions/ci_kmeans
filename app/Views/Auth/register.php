<?= $this->extend('App\Views\Layouts\auth') ?>

<?= $this->section('content') ?>
<div class="container">
    <div class="row">
        <div class="col s12 m6 offset-m3">
            <form id="register_form" method="post" action="<?=route_to('register')?>">
                <?= csrf_field() ?>
                <div class="card z-depth-3">
                    <div class="card-content"> 
                        <span class="card-title" style="font-weight:400;">Register</span>
                        <div class="input-field">
                            <input name="name" id="name" type="text" class="validate" value="<?= old('name') ?>" required>
                            <label for="name">Name</label>
                        </div>
                        <div class="input-field">
                            <input name="email" id="email" type="email" class="validate" value="<?= old('email') ?>" required>
                            <label for="email">Email</label>
                        </div>
                        <div class="input-field">
                            <input name="username" id="username" type="text" class="validate" value="<?= old('username') ?>" required>
                            <label for="username">Username</label>
                        </div>
                        <div class="input-field">
                            <select name="role" id="role" class="validate" required>
                                <option value="user" selected>User</option>
                            </select>
                            <label for="role">Role</label>
                        </div>
                        <div class="input-field">
                            <input name="password" id="password" type="password" class="validate" required>
                            <label for="password">Password</label>
                        </div>
                        <div class="input-field">
                            <input id="pass_confirm" name="pass_confirm" type="password" class="validate" required>
                            <label for="pass_confirm">Confirm Password</label>
                            <span class="helper-text" data-error="Please enter confirm password correctly."></span>
                        </div>
                        <div class="row">
                            <div class="col sm6">
                                <a href="<?=route_to('login')?>">I have an account!</a>
                            </div>
                        </div>
                    </div>
                    <div class="card-action">
                        <button type="submit" class="btn">Register</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    $(document).ready(function(){
        function checkConfirmPassword()
        {
            if ($("#password").val() != $('#pass_confirm').val() || $("#password").val().length < 8) {
                $('#pass_confirm').removeClass("valid").addClass("invalid");
                $('#button-submit').prop('disabled', true)
            } else {
                $('#pass_confirm').removeClass("invalid").addClass("valid");
                $('#button-submit').prop('disabled', false)
            }
        }
        
        $("#password").on("change keyup focusout", function (e) {
            checkConfirmPassword()
        });
        $("#pass_confirm").on("change keyup focusout", function (e) {
            checkConfirmPassword()
        });
    });
</script>
<?= $this->endSection() ?>