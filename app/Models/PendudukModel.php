<?php

namespace App\Models;

class PendudukModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'data_penduduk';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDeletes       = true;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'nik',
		'nama',
		'umur',
		'kredit',
		'pekerjaan',
		'penghasilan',
		'listrik',
		'kondisi_rumah',
		'average',
		'user_id',
		'hasil',
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getFirstCenteroid()
	{
		$datas = $this->findAll();

        $averages = array_column($datas, 'average', 'id');
        $min = min($averages);
        $max = max($averages);
        if(count(array_unique($averages)) >= 3)
        {
            $filtered = array_values(array_filter(array_unique($averages), function($value) use ($min, $max){
                return ($value != $min && $value != $max);
            }));
            $x = $filtered[rand(0,count($filtered)-1)];
        }else{
            $x = $averages[array_rand($averages)];
        }
        $mid = $x;
        $min = json_decode(json_encode($this->find(array_search($min, $averages))), true);
        $mid = json_decode(json_encode($this->find(array_search($mid, $averages))), true);
        $max = json_decode(json_encode($this->find(array_search($max, $averages))), true);
        $r3 = [];
        $r2 = [];
        $r1 = [];
		
		$r3 += ['cluster' => 'C3', 'user_id' => user()->id];
		$r2 += ['cluster' => 'C2', 'user_id' => user()->id];  
		$r1 += ['cluster' => 'C1', 'user_id' => user()->id];  
		
		$columns = [
			"kredit",
			"pekerjaan",
			"penghasilan",
			"listrik",
			"kondisi_rumah",
		];
        foreach($columns as $k)
        {
            $r3 += [$k => $min[$k]];  
            $r2 += [$k => $mid[$k]];  
            $r1 += [$k => $max[$k]];  
        }

        $res = [$r1, $r2, $r3];

		$centeroidPenduduk = new CenteroidPendudukModel();
        $centeroidPenduduk->save($r3);
        $centeroidPenduduk->save($r2);
        $centeroidPenduduk->save($r1);
        
        return $res;
	}

	public function getEuclidian($centeroid)
	{
		helper('array');

		$datas = $this->findAll();

        $res = [];

		$columns = [
			"kredit",
			"pekerjaan",
			"penghasilan",
			"listrik",
			"kondisi_rumah",
		];
        foreach($datas as $data)
        {
			$data = json_decode(json_encode($data), true);
            $a = [];
			foreach($columns as $k)
			{
                $a[] = $data[$k];
			}
			// dd($a);
            $c3 = eucDistance(array_values($centeroid[2]), $a);
            $c2 = eucDistance(array_values($centeroid[1]), $a);
            $c1 = eucDistance(array_values($centeroid[0]), $a);
            
            $min = min([$c1, $c2, $c3]);
            $cluster = '';
            if($min == $c3)
            {
                $cluster = 'C3';
            }
            if($min == $c2)
            {
                $cluster = 'C2';
            }
            if($min == $c1)
            {
                $cluster = 'C1';
            }
            $arr = [
				'data_id' => $data['id'],
                'nik' => $data['nik'],
				'nama' => $data['nama'],
				'user_id' => $data['user_id'],
                'C1' => $c1,
                'C2' => $c2,
                'C3' => $c3,
                'cluster' => $cluster
            ];
			foreach($columns as $k)
			{
                $arr += [$k => $data[$k]];
			}
            $res[] = $arr;
        }

        return $res;
	}

	public function getNewCenteroid($euc)
    {
		$columns = [
			"kredit",
			"pekerjaan",
			"penghasilan",
			"listrik",
			"kondisi_rumah",
		];

        $c3 = [];
        $c2 = [];
        $c1 = [];
        foreach ($euc as $row) {
            if($row['cluster'] === 'C1')
            {
                $c1[] = $row;
            }
            if($row['cluster'] === 'C2')
            {
                $c2[] = $row;
            }
            if($row['cluster'] === 'C3')
            {
                $c3[] = $row;
            }
        }
        
        $e1 = [];
        $e2 = [];
        $e3 = [];
		foreach($columns as $k)
		{
			$e1 += [$k => (count($c1) > 0) ? array_sum(array_column($c1, $k))/count($c1) : 0];
			$e2 += [$k => (count($c2) > 0) ? array_sum(array_column($c2, $k))/count($c2) : 0];
			$e3 += [$k => (count($c3) > 0) ? array_sum(array_column($c3, $k))/count($c3) : 0];
		}
		
        return [$e1,$e2,$e3];
    }

	public function createBatch($array)
    {
        $res = [];
        foreach($array as $row)
        {
			if(!array_key_exists('average', $row))
			{
				$val = [
					$row['kredit'],
					$row['pekerjaan'],
					$row['penghasilan'],
					$row['listrik'],
					$row['kondisi_rumah'],
				];
				$row += ['average' => array_avg($val)];
			}
            if(!$res = $this->save($row))
            {	
                $_SESSION['errors'] = $this->errors();
                return false;
            }
        }

        return $res;
    }
}
