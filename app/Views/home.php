<?= $this->extend('App\Views\Layouts\dashboard') ?>

<?= $this->section('content') ?>
<?php
    echo view('App\Views\Components/breadcrumb', [
        'breadcrumb_items' => [
            [
                'title' => 'Home',
                'link' => 'javascript:void(0)'
            ]
        ]
    ]);

    if(user()->role == 'user' || user()->role == 'admin')
    {
?>
        <div class="card center-align z-depth-2" style="margin-top: 15px;">
            <div class="card-content">
                <span class="card-title">
                SISTEM INFORMASI<br>
                K-MEANS CLUSTERING UNTUK MENGELOMPOKAN PENERIMAAN BANTUAN SOSIAL<br>
                BERDASARKAN INFORMASI PEKERJAAN, PENGHASILAN, LISTRIK, KONDISI RUMAH
                </span>
            </div>
            <div class="card-action">
                <p>
                    Sistem Informasi ini digunakan untuk mengelompokan kelompok penduduk berdasarkan informasi pekerjaan, penghasilan, listrik dan kondisi rumah. Dengan adanya sistem informasi ini diharapkan bisa membantu Dinas Sosial dalam mengambil beberapa kebijakan terkait dengan pemberian bantuan sosial pada masyarakat.
                </p>

                <hr>
                <div id="man" class="col s12">
                    <div class="card material-table z-depth-2">
                        <div class="table-header">
                            <span class="table-title"><?= $tableTitle ?? 'Hasil' ?></span>
                            <div class="actions">
                                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                                </form>
                            </div>
                        </div>
                        <table class="highlight datatable" id="table-hasil">
                            <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($hasil as $key => $row) 
                            {
                            ?>
                                <tr>
                                    <td><?= $row->nik ?></td>
                                    <td><?= $row->nama ?></td>
                                    <td>
                                        <span class="<?= ($row->hasil == 'Lulus Seleksi') ? 'green-text': (($row->hasil == 'Tidak Lulus Seleksi') ? 'red-text' : 'yellos-text')?>">
                                        <?= $row->hasil ?>
                                        </span>    
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
<?php
    }
?>
<?= $this->endSection() ?>