<?= $this->extend('App\Views\Layouts\dashboard') ?>

<?= $this->section('content') ?>
<?php
    echo view('App\Views\Components/breadcrumb', [
        'breadcrumb_items' => [
            [
                'title' => 'Home',
                'link' => base_url('/')
            ],
            [
                'title' => 'Data Penduduk',
                'link' => route_to('penduduk.index')
            ],
            [
                'title' => 'Proses Data',
                'link' => 'javascript:void(0)'
            ]
        ]
    ]);
?>
<div class="card">
    <div class="card-content">
    <?php
        if(count($penduduks) > 0)
        {
            echo view('App\Views\Penduduk/view/table',[
                'tableTitle' => 'Data Awal',
                'datas' => $penduduks,
                'use_action' => false,
                'detail' => false,
            ]);

    ?>
            <!-- Start Proses Data -->
                <div class="center-align">
                    <form action="<?=route_to('penduduk.proses_data.action')?>" method="post">
                        <button type="submit" class="btn orange" name="centeroid">PROSES <?=(count($centeroidPenduduk) > 0) ? 'ULANG' : 'DATA'?></button>
                    </form>
                </div>
            <!-- End Proses Data -->
            <br>
            <!-- Start Iterasi -->
        <?php
            if(count($centeroidPenduduk) > 0)
            {
                $c1 = [];
                $c2 = [];
                $c3 = [];
                foreach ($iterationView as $key => $value) {
        ?>
                    <div class="card z-depth-2">
                        <div class="card-content">
                            <span class="card-title"><strong>Iterasi ke-<?=$value['iteration']?></strong></span>
                            <?= view('App\Views\Penduduk/view/table-centeroid', [
                                'table_centeroid_id' => $value['table_centeroid_id'],
                                'centeroid' => $value['centeroid']
                            ])?>

                            <?= view('App\Views\Penduduk/view/table-euclidean', [
                                'table_euclidean_id' => $value['table_euclidean_id'],
                                'euc' => $value['euc']
                            ])?>
                        </div>
                    </div>
        <?php
                }
        ?>
                <div id="man" class="col s12">
                    <div class="card material-table z-depth-2">
                        <div class="table-header">
                            <span class="table-title">Hasil Clustering</span>
                            <div class="actions">
                                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                            </div>
                        </div>
                        <table class="highlight datatable">
                            <thead>
                                <tr>
                                    <th>NIK</th>
                                    <th>Cluster</th>
                                </tr>
                            </thead>
                            <tbody>
                        <?php
                            foreach ($euc as $row)
                            {
                        ?>
                                <tr>
                                    <td><?=$row['nik']?></td>
                                    <td><?=$row['cluster']?></td>
                                </tr>
                        <?php
                                if($row['cluster'] === 'C1')
                                {
                                    $c1[] = $row;
                                }
                                if($row['cluster'] === 'C2')
                                {
                                    $c2[] = $row;
                                }
                                if($row['cluster'] === 'C3')
                                {
                                    $c3[] = $row;
                                }
                            }
                        ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End Iterasi -->

                <!-- Start Kesimpulan -->

                <div class="card z-depth-3">
                    <div class="card-content">
                        <span class="card-title"><strong>Kesimpulan:</strong></span>
                        <p>Berdasarkan perhitungan menggunakan K-Means Clustering maka didapatkan hasil bahwa anggota yang tidak mampu:</p>
                        <ol>
                        <?php
                            foreach ($c1 as $c) {
                        ?>
                            <li><?=$c['nik']." (".$c['nama'].")"?></li>
                        <?php
                            }
                        ?>
                            <!-- <li>Anggota C1 = <?=implode(', ', array_column($c1, 'nik'))?></li>
                            <li>Anggota C2 = <?=implode(', ', array_column($c2, 'nik'))?></li>
                            <li>Anggota C3 = <?=implode(', ', array_column($c3, 'nik'))?></li> -->
                        </ol>
                        <br>
                        <!-- <hr>
                        <strong>Keterangan:</strong>
                        <ul>
                            <li>C1 = Tidak Mampu</li>
                            <li>C2 = Kurang Mampu</li>
                            <li>C3 = Mampu</li>
                        </ul> -->
                    </div>
                </div>
                <!-- End Kesimpulan -->
    <?php
            }
        }else{
            if(!count($centeroidPenduduk) > 0 && !count($penduduks) > 0)
            {
                echo "Access Denied <br><br><a href='#' class='btn-small' onclick='history.back()'>Back</a>";
            }
        }
    ?>
    </div>
</div>
<?= $this->endSection() ?>