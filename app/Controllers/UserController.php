<?php

namespace App\Controllers;

use Config\Auth as AuthConfig;
use App\Entities\User;
use App\Models\UserModel;

class UserController extends BaseController
{
	/**
	 * @var AuthConfig
	 */
	protected $config;

	public function __construct()
	{
		$this->config = config('Auth');
	}

	public function index()
	{
        $userModel = new UserModel();
        $data['users'] = $userModel->findAll();
		return view('Users/index', $data);
	}

	public function create()
	{
		return view('Users/create');
	}

	public function store()
	{
		$users = model(UserModel::class);

		// Validate basics first since some password rules rely on these fields
		$rules = [
			'username' => 'required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username]',
			'email'    => 'required|valid_email|is_unique[users.email]',
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		// Validate passwords since they can only be validated properly here
		$rules = [
			'password'     => 'required|strong_password',
			'pass_confirm' => 'required|matches[password]',
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}

		// Save the user
		$allowedPostFields = array_merge(['password'], $this->config->validFields, $this->config->personalFields);
		$user = new User($this->request->getPost($allowedPostFields));
		
		$this->config->requireActivation === null ? $user->activate() : $user->generateActivateHash();

		if (! $users->save($user))
		{
			return redirect()->back()->withInput()->with('errors', $users->errors());
		}

		// Success!
		return redirect()->route('users.index')->with('message', 'Success create New User');
	}

	public function edit($id)
	{
		$userModel = new UserModel();
        $data['user'] = $userModel->find($id);
		return view('Users/edit', $data);
	}

	public function update($id)
	{
		$userModel = new UserModel();
		$findUser = $userModel->find($id);
		
		// Validate basics first since some password rules rely on these fields
		$rules = [
			'username' => "required|alpha_numeric_space|min_length[3]|max_length[30]|is_unique[users.username,id,{$id}]",
			'email'    => "required|valid_email|is_unique[users.email,id,{$id}]",
		];

		if (! $this->validate($rules))
		{
			return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
		}
		
		$data = [
			'name' => $this->request->getPost('name'),
			'email' => $this->request->getPost('email'),
			'username' => $this->request->getPost('username'),
			'role' => $this->request->getPost('role')
		];
		
		$user = new User($data);

		if($this->request->getPost('pass_confirm') && $this->request->getPost('pass_confirm') != '')
        {
            if(!$userModel->checkPassword($id, $this->request->getPost('old_password')))
            {
				return redirect()->back()->withInput()->with('warning', 'Old Password not correct!');
            }

			// Validate passwords since they can only be validated properly here
			$rules = [
				'password'     => 'required|strong_password',
				'pass_confirm' => 'required|matches[password]',
			];
			if (! $this->validate($rules))
			{
				return redirect()->back()->withInput()->with('errors', $this->validator->getErrors());
			}
			$user->setPassword($this->request->getPost('password'));
        }
		

		// $user->setAttributes(array_merge(['id' => $id], $user->toArray()));
		// dd($findUser);

		$data = $user->toArray();
		unset($data[0]);
		if($data['username'] == $findUser->username){
			unset($data['username']);
		}
		if($data['email'] == $findUser->email){
			unset($data['email']);
		}
		if( !$userModel->update($id, $data))
		{
			return redirect()->back()->withInput()->with('errors', $userModel->errors());
		}

		// Success!
		return redirect()->route('users.edit', [$id])->with('message', 'Success edit User');
	}

	public function destroy($id)
	{
		$userModel = new UserModel();
        $userModel->delete($id);
		$userModel->purgeDeleted();

		// Success!
		return redirect()->route('users.index')->with('message', 'Success delete User');
	}
}
