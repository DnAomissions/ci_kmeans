<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->group('', ['filter' => 'login'], function($routes){
	$routes->get('/', 'Home::index');

	// Users
	$routes->get('/users', 'UserController::index', ['as' => 'users.index']);
	$routes->get('/users/create', 'UserController::create', ['as' => 'users.create']);
	$routes->post('/users/store', 'UserController::store', ['as' => 'users.store']);
	$routes->get('/users/edit/(:num)', 'UserController::edit/$1', ['as' => 'users.edit']);
	$routes->post('/users/update/(:num)', 'UserController::update/$1', ['as' => 'users.update']);
	$routes->post('/users/destroy/(:num)', 'UserController::destroy/$1', ['as' => 'users.destroy']);

	// Penduduk
	$routes->get('/penduduk', 'PendudukController::index', ['as' => 'penduduk.index']);
	$routes->get('/penduduk/create', 'PendudukController::create', ['as' => 'penduduk.create']);
	$routes->post('/penduduk/store', 'PendudukController::store', ['as' => 'penduduk.store']);
	$routes->get('/penduduk/edit/(:num)', 'PendudukController::edit/$1', ['as' => 'penduduk.edit']);
	$routes->post('/penduduk/update/(:num)', 'PendudukController::update/$1', ['as' => 'penduduk.update']);
	$routes->post('/penduduk/destroy/(:num)', 'PendudukController::destroy/$1', ['as' => 'penduduk.destroy']);
	$routes->post('/penduduk/truncate', 'PendudukController::truncate', ['as' => 'penduduk.truncate']);
	
	$routes->get('/penduduk/export', 'PendudukController::export', ['as' => 'penduduk.export']);
	$routes->post('/penduduk/import', 'PendudukController::import', ['as' => 'penduduk.import']);

	$routes->get('/penduduk/proses_data', 'PendudukController::proses_data', ['as' => 'penduduk.proses_data']);
	$routes->post('/penduduk/proses_data/action', 'PendudukController::proses_data_action', ['as' => 'penduduk.proses_data.action']);
});

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
