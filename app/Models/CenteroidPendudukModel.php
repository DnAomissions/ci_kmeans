<?php

namespace App\Models;

class CenteroidPendudukModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'centeroid_penduduk';
	protected $primaryKey           = 'id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDeletes       = true;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'cluster',
		'kredit',
		'pekerjaan',
		'penghasilan',
		'listrik',
		'kondisi_rumah',
		'user_id'
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'datetime';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public function getCenteroid()
    {
        $c3 = $this->where('cluster', 'C3')->findAll()[0];
        $c2 = $this->where('cluster', 'C2')->findAll()[0];
        $c1 = $this->where('cluster', 'C1')->findAll()[0];
        $r1 = [];
        $r2 = [];
        $r3 = [];

		$columns = [
			"kredit",
			"pekerjaan",
			"penghasilan",
			"listrik",
			"kondisi_rumah",
		];

        // C1
		foreach($columns as $k)
		{
			$r1 += [$k => $c1[$k]];
		}

        // C2
		foreach($columns as $k)
		{
            $r2 += [$k => $c2[$k]];
		}
        
        // C3
		foreach($columns as $k)
		{
            $r3 += [$k => $c3[$k]];
		}

        $res = [$r1, $r2, $r3];

        return $res;
    }
}
