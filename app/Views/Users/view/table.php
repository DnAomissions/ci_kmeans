<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title">Data Users</span>
            <div class="actions">
                <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
            </div>
        </div>
        <table class="highlight datatable">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Username</th>
                    <th>Role</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($users as $key => $row) 
            {
            ?>
                <tr>
                    <td><?=$row->name?></td>
                    <td><?=$row->email?></td>
                    <td><?=$row->username?></td>
                    <td><?=ucfirst($row->role)?></td>
                    <td>
                        <form action="<?=route_to('users.destroy', $row->id)?>" class="form-destroy" onsubmit="return confirm('Are you sure to delete <?=$row->name ?>?');" method="post">
                            <a href="<?=route_to('users.edit', $row->id)?>" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Edit"><i class="material-icons blue-text">edit</i></a>
                        <?php
                            if(user()->id != $row->id)
                            {
                        ?>
                            <input name="id" id="id" value="<?= $row->id ?? ''?>" type="hidden"/>
                            <button type="submit" name="destroy" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Delete"><i class="material-icons red-text">delete_forever</i></button>
                        <?php
                            }
                        ?>
                        </form>
                    </td>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>