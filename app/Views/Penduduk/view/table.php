<?php
    $pekerjaan = [
        "1" => "PNS",
        "2" => "Wirausaha",
        "3" => "Wiraswasta",
        "4" => "Tidak Bekerja",
    ];
    $penghasilan = [
        "1" => "> Rp. 5.000.000",
        "2" => "Rp. 3.000.000 < Rp. 5.000.000",
        "3" => "Rp. 1.000.000 < Rp. 3.000.000",
        "4" => "Rp. 500.000 < Rp. 1.000.000",
        "5" => "< Rp. 500.000",
    ];
    $listrik = [
        "1" => "> 1.300 watt",
        "2" => "> 1.200 watt",
        "3" => "< 1.200 watt",
        "4" => "< 900 watt",
        "5" => "< 750 watt",
    ];
    $kondisi_rumah = [
        "1" => "Sangat Mampu",
        "2" => "Mampu",
        "3" => "Kurang Mampu",
        "4" => "Tidak Mampu",
    ];
?>
<div id="man" class="col s12">
    <div class="card material-table z-depth-2">
        <div class="table-header">
            <span class="table-title"><?= $tableTitle ?? 'Data Penduduk' ?></span>
            <div class="actions">
                <form action="<?=route_to('penduduk.truncate')?>" class="form-truncate" onsubmit="return confirm('Are you sure to clear all data?');" method="post">
                    <button type="button" class="waves-effect waves-grey green-text btn-flat table-detail-trigger" data-table="table-produksi">Detail</button>
            <?php
                if(count($datas) > 0 && $use_action && user()->role == 'admin'){
            ?>
                    <button type="submit" name="truncate" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Clear All"><i class="material-icons red-text">delete_sweep</i></button>
            <?php
                }
            ?>
                    <a href="#" class="search-toggle waves-effect btn-flat nopadding"><i class="material-icons">search</i></a>
                </form>
            </div>
        </div>
        <table class="highlight datatable" id="table-produksi">
            <thead>
                <tr>
                    <th>NIK</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Kredit</th>
                    <th>Pekerjaan</th>
                    <th>Penghasilan</th>
                    <th>Listrik</th>
                    <th>Kondisi Rumah</th>
                    <!-- <th>Rata-rata</th> -->
            <?php
                if($use_action)
                {
            ?>
                    <th class="table-column-action">Action</th>
            <?php
                }
            ?>
                </tr>
            </thead>
            <tbody>
            <?php
            foreach ($datas as $key => $row) 
            {
            ?>
                <tr>
                    <td><?= $row->nik ?></td>
                    <td><?= $row->nama ?></td>
                    <td><?= $row->umur ?></td>
            <?php
                
                
                if($detail)
                {
                    if($row->kredit == 1)
                    {
            ?>
                        <td><span class="green-text"><i class="material-icons">check</i></span></td>
            <?php
                    }else{
            ?>
                        <td><span class="red-text"><i class="material-icons">clear</i></span></td>
            <?php
                    }
            ?>
                <td><?= $pekerjaan[$row->pekerjaan] ?></td>
                <td><?= $penghasilan[$row->penghasilan] ?></td>
                <td><?= $listrik[$row->listrik] ?></td>
                <td><?= $kondisi_rumah[$row->kondisi_rumah] ?></td>
            <?php
                }else{
            ?>
                <td><?= $row->kredit ?></td>
                <td><?= $row->pekerjaan ?></td>
                <td><?= $row->penghasilan ?></td>
                <td><?= $row->listrik ?></td>
                <td><?= $row->kondisi_rumah ?></td>
            <?php
                }
            ?>
                <!-- <td><?=$row->average?></td> -->
            <?php
                if($use_action)
                {
            ?>
                    <td class="table-column-action">
                        <form action="<?=route_to('penduduk.destroy', $row->id)?>" class="form-destroy" onsubmit="return confirm('Are you sure to delete <?= $row->nama ?>?');" method="post">
                            <input name="id" id="id" value="<?= $row->id ?? ''?>" type="hidden"/>
                            <a href="<?=route_to('penduduk.edit', $row->id)?>" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Edit"><i class="material-icons blue-text">edit</i></a>
                            <button type="submit" name="destroy" class="btn-flat tooltipped" data-position="bottom" data-tooltip="Delete"><i class="material-icons red-text">delete_forever</i></button>
                        </form>
                    </td>
            <?php
                }
            ?>
                </tr>
            <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</div>