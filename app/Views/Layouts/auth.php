<?= $this->extend('App\Views\Layouts\app') ?>

<?= $this->section('pageStyles') ?>
    <style>
        html, body, .auth-form {
            height: 100%;
        }
    </style>
    <?= $this->renderSection('style') ?>
<?= $this->endSection() ?>

<?= $this->section('app') ?>
    <div class="auth-form valign-wrapper">
        <?= $this->renderSection('content') ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('pageScripts') ?>
    <?= $this->renderSection('script') ?>
<?= $this->endSection() ?>