<?php namespace App\Models;

use Myth\Auth\Models\UserModel as MythModel;
use Myth\Auth\Password;

class UserModel extends MythModel
{
    protected $allowedFields = [
        'email', 'username', 'password_hash', 'reset_hash', 'reset_at', 'reset_expires', 'activate_hash',
        'status', 'status_message', 'active', 'force_pass_reset', 'permissions', 'deleted_at',
        'name', 
        'role',
    ];

    protected $returnType = 'App\Entities\User';

    function checkPassword($id, $password, $returnUser = false)
    {
        $user = $this->find($id);

        // Now, try matching the passwords.
        if (! Password::verify($password, $user->password_hash))
        {
            $this->error = lang('Auth.invalidPassword');
            return false;
        }

        return $returnUser
            ? $user
            : true;
    }
}
