<?= $this->extend('App\Views\Layouts\dashboard') ?>

<?= $this->section('content') ?>
<?php
    echo view('App\Views\Components/breadcrumb', [
        'breadcrumb_items' => [
            [
                'title' => 'Home',
                'link' => base_url('/')
            ],
            [
                'title' => 'Data Penduduk',
                'link' => 'javascript:void(0)'
            ]
        ]
    ]);

    // Floating Button Setup
    $button_items = [];
    
    if(user()->role == 'user')
    {
        array_unshift($button_items, [
            'name' => 'Input Manual',
            'icon' => 'add',
            'class' => 'green',
            'link' => route_to('penduduk.create')
        ]);

        array_unshift($button_items, [
            'name' => 'Import Data',
            'icon' => 'insert_drive_file',
            'class' => 'blue modal-trigger',
            'link' => '#modal-penduduk'
        ]);
    }

    array_unshift($button_items, [
        'name' => 'Export Data',
        'icon' => 'file_download',
        'class' => 'purple',
        'link' => route_to('penduduk.export')
    ]);

    if(count($penduduks) > 0 && user()->role == 'admin')
    {
        array_unshift($button_items, [
            'name' => 'Proses Data',
            'icon' => 'cached',
            'class' => 'orange',
            'link' => route_to('penduduk.proses_data')
        ]);
    }

    echo view('App\Views\Components/floating-button', [
        'button_items' => $button_items
    ]);
    
    echo view('App\Views\Penduduk/view/table',[
        'datas' => $penduduks,
        'use_action' => true,
        'detail' => true,
    ]);

    echo view('App\Views\Components/modal-import', [
        'modal' => [
            'id' => 'modal-penduduk',
            'model' => 'penduduk',
            'text' => 'Excel Template : <a href="/template/penduduk-template.xlsx" download/>penduduk-template.xlsx</a>',
            'action' => base_url('/penduduk/import'),
        ]
    ]);
?>    
<?= $this->endSection() ?>