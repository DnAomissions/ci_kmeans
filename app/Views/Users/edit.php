<?= $this->extend('App\Views\Layouts\dashboard') ?>

<?= $this->section('content') ?>
<?php
    // Role Permission 
    if(user()->role != 'admin')
    {
        echo "<script>window.location.replace('".base_url('/')."')</script>";
        exit;
    }

    echo view('App\Views\Components/breadcrumb', [
        'breadcrumb_items' => [
            [
                'title' => 'Home',
                'link' => base_url('/')
            ],
            [
                'title' => 'Users',
                'link' => route_to('users.index')
            ],
            [
                'title' => $user->name,
                'link' => 'javascript:void(0)'
            ],
        ]
    ]);

    echo view('App\Views\Users/view/form', [
        'user' => $user
    ]);
?>    
<?= $this->endSection() ?>